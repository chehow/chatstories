export default [
    {
        "id": "1",
        "title": "Не открывай",
        "description": "Обычная вечеринка превратилась в кошмар…",
        "author": "Ира Логинова",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/011/original/photo-1444792131309-2e517032ded6?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=a67284f4b7bf5c95e00d8f6046f2d81b",
        "language": "ru",
        "updatedAt": "2018-11-10T12:26:15.652Z",
        "estimate": 5,
        "category": "Мистика",
        "new": true,
        "episodes": [
            {
                "id": "1",
                "title": "Эпизод 1",
                "messages": [
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А может… не надо?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },

                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Джереми, какого черта ты позвал ее с нами?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://placemat.imgix.net/placeholder_images/images/000/000/039/original/photo-1439694458393-78ecf14da7f9?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=fd335fe2e88280ce3439468dbf49f7f0"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Она меня бесит своим нытьем!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эй, полегче!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Шла бы ты домой к мамочке."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И братца своего захвати."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никуда я не пойду! Я же сказал, я в теме."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "А папка не наругает?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никто об этом не узнает."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Конечно, не узнает!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Если кто-то из вас проболтается, урою обоих, ясно?"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Хватит тебе уже! Давай, доставай."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Как я и обещал, улетная штука."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Взял у другана, все чисто."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ты так и в прошлый раз говорил."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Это точно годная тема!"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Сколько брать?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Возьми одну сначала, чуть что потом еще."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "А я сразу две."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "И я."
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ты что!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Эээ, верни!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да ладно, не жмись, дай и ей оттянуться."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Джесс, держи."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Тоня, если не хочешь…"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я и не собиралась!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я здесь только из-за Кевина."
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Смотри-ка, Кевин привел свою няньку!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": " Так, и когда должно вштырить?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да почти сразу, жди!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Да? А я нихрена не чувствую!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Нет, что-то есть…"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Тссс! Вы это слышите?"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Как будто кто-то снаружи…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Да, я тоже слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да прекратите, ничего там нет!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Это все глюки, ясно? Довольны?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Нет…Так не бывает…"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Он сказал, будет просто клево, но это…Там точно кто-то есть!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Теперь и я слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Дверь! Закройте дверь! Быстрее!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ну все, с меня хватит! Кевин, мы идем домой!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Ты что, сдурела? Я не пойду туда!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Господи, Кевин, хватит! Это все не настоящее, понимаешь? Тебя просто накрыло!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Посмотри на себя!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Заткнись!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Мы больше не откроем эту дверь, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И что, вы так и будете сидеть здесь всю ночь??"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Мы будем сидеть здесь, пока эти твари не свалят!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Какие твари?! Да нет там никого!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я иду домой без тебя, мне плевать!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И покажу это видео маме с папой, ты понял?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Отойди от двери, сучка!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Реально, только попробуй!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А то что? Уроешь меня?"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да пошли вы!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Я сказала, отвали!!!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эээ, ты чего?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Никто не подойдет к двери, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ясно, но зачем ты толкнула ее?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Какого черта?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Вроде дышит"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Хоть заткнулась."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Повезло. Теперь она не слышит их!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Сопли подбери! Меня тоже достал этот скрежет!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И долбаный лай!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Они воют!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Черт, что за херня??? Помогите!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Что с тобой?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "По мне что-то ползет! Уберите это!!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Где?? Я ничего не вижу!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Мое ухо! Оно заползло прямо в ухо!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Фууу! Гадость! Брайан, сделай же что-нибудь!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Скорее!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Сейчас! Вот тааак…"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ааааааааааааа!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Все! Я его снял! Смотри!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Какая мерзкая тварь! Блин, спасибо!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Ты как, Джер?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Норм. Я сейчас чуть-чуть полежу…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Что это за звук?!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Не обосрись, это мой мобильник."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Блин, мне этот друган пишет. Зацените."
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Блин, чувак, ты уже пробовал?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ну да, все ништяк, но тут какие-то твари снаружи, мы не можем выбраться."
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Чувак, срочно вызывай скорую, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ты че!!! Чтобы они нас запалили!"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН НЕ В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Да он что, вообще долбанулся?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Нельзя никого впускать!!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Да, не открывай! Никому! Не хватало еще, чтобы они все сюда приползли!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Покажи ему ту тварь, которая пыталась убить Джереми!"
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Смотри, что здесь!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Снял прямо с башки Джера!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "info",
                        "text": "КОНЕЦ ЭПИЗОДА 1"
                    },
                    {
                        "type": "info",
                        "text": "ПРОДОЛЖЕНИЕ СЛЕДУЕТ!"
                    }
                ]
            },
            {
                "id": "2",
                "title": "Эпизод 2",
                "messages": [
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А может… не надо?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },

                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Джереми, какого черта ты позвал ее с нами?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://placemat.imgix.net/placeholder_images/images/000/000/039/original/photo-1439694458393-78ecf14da7f9?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=fd335fe2e88280ce3439468dbf49f7f0"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Она меня бесит своим нытьем!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эй, полегче!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Шла бы ты домой к мамочке."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И братца своего захвати."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никуда я не пойду! Я же сказал, я в теме."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "А папка не наругает?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никто об этом не узнает."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Конечно, не узнает!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Если кто-то из вас проболтается, урою обоих, ясно?"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Хватит тебе уже! Давай, доставай."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Как я и обещал, улетная штука."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Взял у другана, все чисто."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ты так и в прошлый раз говорил."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Это точно годная тема!"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Сколько брать?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Возьми одну сначала, чуть что потом еще."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "А я сразу две."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "И я."
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ты что!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Эээ, верни!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да ладно, не жмись, дай и ей оттянуться."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Джесс, держи."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Тоня, если не хочешь…"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я и не собиралась!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я здесь только из-за Кевина."
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Смотри-ка, Кевин привел свою няньку!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": " Так, и когда должно вштырить?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да почти сразу, жди!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Да? А я нихрена не чувствую!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Нет, что-то есть…"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Тссс! Вы это слышите?"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Как будто кто-то снаружи…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Да, я тоже слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да прекратите, ничего там нет!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Это все глюки, ясно? Довольны?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Нет…Так не бывает…"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Он сказал, будет просто клево, но это…Там точно кто-то есть!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Теперь и я слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Дверь! Закройте дверь! Быстрее!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ну все, с меня хватит! Кевин, мы идем домой!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Ты что, сдурела? Я не пойду туда!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Господи, Кевин, хватит! Это все не настоящее, понимаешь? Тебя просто накрыло!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Посмотри на себя!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Заткнись!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Мы больше не откроем эту дверь, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И что, вы так и будете сидеть здесь всю ночь??"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Мы будем сидеть здесь, пока эти твари не свалят!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Какие твари?! Да нет там никого!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я иду домой без тебя, мне плевать!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И покажу это видео маме с папой, ты понял?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Отойди от двери, сучка!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Реально, только попробуй!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А то что? Уроешь меня?"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да пошли вы!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Я сказала, отвали!!!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эээ, ты чего?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Никто не подойдет к двери, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ясно, но зачем ты толкнула ее?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Какого черта?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Вроде дышит"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Хоть заткнулась."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Повезло. Теперь она не слышит их!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Сопли подбери! Меня тоже достал этот скрежет!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И долбаный лай!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Они воют!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Черт, что за херня??? Помогите!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Что с тобой?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "По мне что-то ползет! Уберите это!!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Где?? Я ничего не вижу!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Мое ухо! Оно заползло прямо в ухо!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Фууу! Гадость! Брайан, сделай же что-нибудь!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Скорее!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Сейчас! Вот тааак…"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ааааааааааааа!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Все! Я его снял! Смотри!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Какая мерзкая тварь! Блин, спасибо!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Ты как, Джер?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Норм. Я сейчас чуть-чуть полежу…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Что это за звук?!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Не обосрись, это мой мобильник."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Блин, мне этот друган пишет. Зацените."
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Блин, чувак, ты уже пробовал?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ну да, все ништяк, но тут какие-то твари снаружи, мы не можем выбраться."
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Чувак, срочно вызывай скорую, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ты че!!! Чтобы они нас запалили!"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН НЕ В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Да он что, вообще долбанулся?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Нельзя никого впускать!!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Да, не открывай! Никому! Не хватало еще, чтобы они все сюда приползли!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Покажи ему ту тварь, которая пыталась убить Джереми!"
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Смотри, что здесь!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Снял прямо с башки Джера!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "info",
                        "text": "КОНЕЦ ЭПИЗОДА 1"
                    },
                    {
                        "type": "info",
                        "text": "ПРОДОЛЖЕНИЕ СЛЕДУЕТ!"
                    }
                ]
            }
        ]
    },
    {
        "id": "2",
        "title": "Вечер",
        "description": "Обычная вечеринка превратилась в кошмар…",
        "author": "Ира Логинова",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/216/original/14520142738_fa72e087a4_b.jpg?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=581478c8d05b5cdfacef1153509cfc06",
        "language": "ru",
        "updatedAt": "2018-11-10T12:26:15.652Z",
        "estimate": 5,
        "category": "Роман",
        "top": true,
        "episodes": [
            {
                "id": "1",
                "title": "Эпизод 1",
                "messages": [
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А может… не надо?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },

                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Джереми, какого черта ты позвал ее с нами?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://placemat.imgix.net/placeholder_images/images/000/000/039/original/photo-1439694458393-78ecf14da7f9?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=fd335fe2e88280ce3439468dbf49f7f0"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Она меня бесит своим нытьем!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эй, полегче!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Шла бы ты домой к мамочке."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И братца своего захвати."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никуда я не пойду! Я же сказал, я в теме."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "А папка не наругает?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никто об этом не узнает."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Конечно, не узнает!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Если кто-то из вас проболтается, урою обоих, ясно?"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Хватит тебе уже! Давай, доставай."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Как я и обещал, улетная штука."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Взял у другана, все чисто."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ты так и в прошлый раз говорил."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Это точно годная тема!"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Сколько брать?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Возьми одну сначала, чуть что потом еще."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "А я сразу две."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "И я."
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ты что!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Эээ, верни!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да ладно, не жмись, дай и ей оттянуться."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Джесс, держи."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Тоня, если не хочешь…"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я и не собиралась!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я здесь только из-за Кевина."
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Смотри-ка, Кевин привел свою няньку!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": " Так, и когда должно вштырить?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да почти сразу, жди!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Да? А я нихрена не чувствую!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Нет, что-то есть…"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Тссс! Вы это слышите?"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Как будто кто-то снаружи…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Да, я тоже слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да прекратите, ничего там нет!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Это все глюки, ясно? Довольны?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Нет…Так не бывает…"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Он сказал, будет просто клево, но это…Там точно кто-то есть!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Теперь и я слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Дверь! Закройте дверь! Быстрее!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ну все, с меня хватит! Кевин, мы идем домой!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Ты что, сдурела? Я не пойду туда!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Господи, Кевин, хватит! Это все не настоящее, понимаешь? Тебя просто накрыло!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Посмотри на себя!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Заткнись!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Мы больше не откроем эту дверь, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И что, вы так и будете сидеть здесь всю ночь??"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Мы будем сидеть здесь, пока эти твари не свалят!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Какие твари?! Да нет там никого!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я иду домой без тебя, мне плевать!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И покажу это видео маме с папой, ты понял?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Отойди от двери, сучка!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Реально, только попробуй!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А то что? Уроешь меня?"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да пошли вы!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Я сказала, отвали!!!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эээ, ты чего?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Никто не подойдет к двери, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ясно, но зачем ты толкнула ее?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Какого черта?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Вроде дышит"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Хоть заткнулась."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Повезло. Теперь она не слышит их!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Сопли подбери! Меня тоже достал этот скрежет!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И долбаный лай!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Они воют!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Черт, что за херня??? Помогите!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Что с тобой?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "По мне что-то ползет! Уберите это!!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Где?? Я ничего не вижу!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Мое ухо! Оно заползло прямо в ухо!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Фууу! Гадость! Брайан, сделай же что-нибудь!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Скорее!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Сейчас! Вот тааак…"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ааааааааааааа!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Все! Я его снял! Смотри!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Какая мерзкая тварь! Блин, спасибо!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Ты как, Джер?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Норм. Я сейчас чуть-чуть полежу…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Что это за звук?!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Не обосрись, это мой мобильник."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Блин, мне этот друган пишет. Зацените."
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Блин, чувак, ты уже пробовал?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ну да, все ништяк, но тут какие-то твари снаружи, мы не можем выбраться."
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Чувак, срочно вызывай скорую, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ты че!!! Чтобы они нас запалили!"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН НЕ В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Да он что, вообще долбанулся?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Нельзя никого впускать!!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Да, не открывай! Никому! Не хватало еще, чтобы они все сюда приползли!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Покажи ему ту тварь, которая пыталась убить Джереми!"
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Смотри, что здесь!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Снял прямо с башки Джера!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "info",
                        "text": "КОНЕЦ ЭПИЗОДА 1"
                    },
                    {
                        "type": "info",
                        "text": "ПРОДОЛЖЕНИЕ СЛЕДУЕТ!"
                    }
                ]
            }
        ]
    },
    {
        "id": "3",
        "title": "Утро",
        "description": "Обычная вечеринка превратилась в кошмар…",
        "author": "Ира Логинова",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/046/original/photo-1438978280647-f359d95ebda4?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=658010d635b5e7e744ccbdec80442eae",
        "language": "ru",
        "updatedAt": "2018-11-10T12:26:15.652Z",
        "estimate": 5,
        "category": "Роман",
        "top": true,
        "episodes": [
            {
                "id": "1",
                "title": "Эпизод 1",
                "messages": [
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А может… не надо?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },

                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Джереми, какого черта ты позвал ее с нами?"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://placemat.imgix.net/placeholder_images/images/000/000/039/original/photo-1439694458393-78ecf14da7f9?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=fd335fe2e88280ce3439468dbf49f7f0"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Она меня бесит своим нытьем!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эй, полегче!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Шла бы ты домой к мамочке."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И братца своего захвати."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никуда я не пойду! Я же сказал, я в теме."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "А папка не наругает?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Никто об этом не узнает."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Конечно, не узнает!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Если кто-то из вас проболтается, урою обоих, ясно?"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Хватит тебе уже! Давай, доставай."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Как я и обещал, улетная штука."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Взял у другана, все чисто."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ты так и в прошлый раз говорил."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Это точно годная тема!"
                    },
                    {
                        "type": "image",
                        "person": "Брайан",
                        "contentURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Сколько брать?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Возьми одну сначала, чуть что потом еще."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "А я сразу две."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "И я."
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ты что!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Эээ, верни!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да ладно, не жмись, дай и ей оттянуться."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Джесс, держи."
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Тоня, если не хочешь…"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я и не собиралась!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я здесь только из-за Кевина."
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Смотри-ка, Кевин привел свою няньку!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": " Так, и когда должно вштырить?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Да почти сразу, жди!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Я, кажется, уже что-то чувствую.",
                        "backgroundImage": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Да? А я нихрена не чувствую!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Нет, что-то есть…"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Тссс! Вы это слышите?"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Как будто кто-то снаружи…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Да, я тоже слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да прекратите, ничего там нет!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Это все глюки, ясно? Довольны?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Нет…Так не бывает…"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Он сказал, будет просто клево, но это…Там точно кто-то есть!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Теперь и я слышу!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Дверь! Закройте дверь! Быстрее!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Ну все, с меня хватит! Кевин, мы идем домой!"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Ты что, сдурела? Я не пойду туда!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Господи, Кевин, хватит! Это все не настоящее, понимаешь? Тебя просто накрыло!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Посмотри на себя!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Заткнись!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Мы больше не откроем эту дверь, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И что, вы так и будете сидеть здесь всю ночь??"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Мы будем сидеть здесь, пока эти твари не свалят!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Какие твари?! Да нет там никого!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Я иду домой без тебя, мне плевать!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "И покажу это видео маме с папой, ты понял?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Отойди от двери, сучка!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Реально, только попробуй!"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "А то что? Уроешь меня?"
                    },
                    {
                        "type": "message",
                        "person": "Тоня",
                        "message": "Да пошли вы!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Я сказала, отвали!!!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Эээ, ты чего?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Никто не подойдет к двери, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ясно, но зачем ты толкнула ее?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Какого черта?"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Вроде дышит"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Хоть заткнулась."
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Повезло. Теперь она не слышит их!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Сопли подбери! Меня тоже достал этот скрежет!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "И долбаный лай!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Они воют!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Черт, что за херня??? Помогите!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Что с тобой?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "По мне что-то ползет! Уберите это!!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Где?? Я ничего не вижу!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Мое ухо! Оно заползло прямо в ухо!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Фууу! Гадость! Брайан, сделай же что-нибудь!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Скорее!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Сейчас! Вот тааак…"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Ааааааааааааа!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Все! Я его снял! Смотри!"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Какая мерзкая тварь! Блин, спасибо!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Ты как, Джер?"
                    },
                    {
                        "type": "message",
                        "person": "Джереми",
                        "message": "Норм. Я сейчас чуть-чуть полежу…"
                    },
                    {
                        "type": "message",
                        "person": "Кевин",
                        "message": "Что это за звук?!!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Не обосрись, это мой мобильник."
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Блин, мне этот друган пишет. Зацените."
                    },
                    {
                        "type": "info",
                        "text": "ЧАТ С ДЖЕЙЕМ"
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Блин, чувак, ты уже пробовал?"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ну да, все ништяк, но тут какие-то твари снаружи, мы не можем выбраться."
                    },
                    {
                        "type": "message",
                        "person": "Джей",
                        "message": "Чувак, срочно вызывай скорую, ясно?!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Ты че!!! Чтобы они нас запалили!"
                    },
                    {
                        "type": "editMessage",
                        "person": "Джей",
                        "message": [
                            "Я облажался, эти таблетки…",
                            "Я тебе говорю, быстро!"
                        ]
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН НЕ В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Да он что, вообще долбанулся?!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Нельзя никого впускать!!"
                    },
                    {
                        "type": "message",
                        "person": "Джесс",
                        "message": "Да, не открывай! Никому! Не хватало еще, чтобы они все сюда приползли!"
                    },
                    {
                        "type": "message",
                        "person": "Синди",
                        "message": "Покажи ему ту тварь, которая пыталась убить Джереми!"
                    },
                    {
                        "type": "info",
                        "text": "БРАЙАН В СЕТИ"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Смотри, что здесь!"
                    },
                    {
                        "type": "message",
                        "person": "Брайан",
                        "message": "Снял прямо с башки Джера!"
                    },
                    {
                        "type": "video",
                        "person": "Тоня",
                        "contentURL": "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
                        "previewURL": "https://firebasestorage.googleapis.com/v0/b/real-chat-stories.appspot.com/o/photo-1443926818681-717d074a57af.jpg?alt=media&token=ec5df8da-5446-4008-9279-3025d7c9dc62"
                    },
                    {
                        "type": "info",
                        "text": "КОНЕЦ ЭПИЗОДА 1"
                    },
                    {
                        "type": "info",
                        "text": "ПРОДОЛЖЕНИЕ СЛЕДУЕТ!"
                    }
                ]
            }
        ]
    }
];

