import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {StoryModel} from '../models/story.model';
import {firestore} from "nativescript-plugin-firebase";
import CollectionReference = firestore.CollectionReference;
import {UserModel} from "~/core/models/user.model";

const firebase = require("nativescript-plugin-firebase/app");

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private _user: UserModel;

    getUser(): UserModel {
        if (!this._user) {
            this._user = new UserModel();
        }

        return this._user;
    }
}