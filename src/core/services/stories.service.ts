import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {StoryModel} from '../models/story.model';
import {firestore} from "nativescript-plugin-firebase";
import CollectionReference = firestore.CollectionReference;

const firebase = require("nativescript-plugin-firebase/app");

@Injectable({
    providedIn: 'root'
})
export class StoriesService {
    private storiesCollection: CollectionReference = firebase.firestore().collection("stories");
    private storiesModels: StoryModel[];

    constructor(
        private apiService: ApiService
    ) {
    }

    async getList(): Promise<StoryModel[]> {
        if (this.storiesModels) {
            return this.storiesModels;
        }
        let querySnapshot = await this.storiesCollection.get();

        let items = [];
        querySnapshot.forEach(doc => {
            items.push(doc.data());
        });

        this.storiesModels = items.map(modelData => this.createModel(modelData));

        return this.storiesModels;
    }

    async getItem(id: String): Promise<StoryModel> {
        let doc = await this.storiesCollection.doc(`${id}`).get();
        return this.createModel(doc.data());
    }

    async getItemInfo(id: String): Promise<StoryModel> {
        return (await this.getList()).find(item => item.id === id);
    }

    private createModel(modelData: any): StoryModel {
        let model = new StoryModel();
        model.deserialize(modelData);
        return model;
    }
}