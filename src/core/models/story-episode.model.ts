import {Model} from "./model";
import {StoryMessageModel} from "~/core/models/story-message.model";

export class StoryEpisodeModel extends Model {
    id: string;
    title: string;
    messages: StoryMessageModel[];

    deserialize(data: any): StoryEpisodeModel {
        data.messages = data.messages.map(message => new StoryMessageModel(this).deserialize(message));
        super.deserialize(data);
        return this;
    }

    persons() {
        return this.messages.map(message => message.person);
    }
}