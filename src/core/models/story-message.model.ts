import {Model} from "./model";
import {StoryEpisodeModel} from "./story-episode.model";

export class StoryMessageModel extends Model {
    id: string;
    index: string;
    person: string;
    message: any;
    currentMessage: string = '';
    isActive: boolean = false;
    type?: string = 'text';
    contentURL?: string = null;
    previewURL?: string = null;
    backgroundImage?: string = null;
    preview?: boolean = false;
    episode: StoryEpisodeModel;
    stop: boolean = false;
    started: boolean = false;
    text: string = '';

    constructor(episode: StoryEpisodeModel) {
        super();
        this.episode = episode;
    }

    deserialize(data: Object): StoryMessageModel {
        Object.assign(this, data);
        return this;
    }

    get prev(): StoryMessageModel | null {
        let index = this.episode.messages.indexOf(this);
        return this.episode.messages[index - 1] || null;
    }

    get align(): number {
        if (!this.prev) {
            return -1;
        }

        if (this.prev.person === this.person) {
            return this.prev.align;
        } else {
            return this.prev.align * -1;
        }
    }

}