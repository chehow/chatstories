import {StoryMessageModel} from './story-message.model';
import {Model} from "./model";
import {StoryEpisodeModel} from './story-episode.model';

export class StoryModel extends Model {
    id: string;
    title: string = '';
    description: string;
    author: string;
    image: string;
    updatedAt: string;
    favorited: boolean;
    category: string;
    estimate: number;
    new: boolean = false;
    top: boolean = false;
    episodes: StoryEpisodeModel[];

    deserialize(data: any): StoryModel {
        data.episodes = data.episodes.map(message => new StoryEpisodeModel().deserialize(message));
        super.deserialize(data);
        return this;
    }
}