import {Model} from "./model";
import {StoryModel} from "~/core/models/story.model";

export class CategoryModel extends Model {
    id: string;
    title: string = '';
    stories: StoryModel[];

    deserialize(data: any): CategoryModel {
        super.deserialize(data);
        return this;
    }
}