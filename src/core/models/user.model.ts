import {Model} from "~/core/models/model";

const localStorage = require("nativescript-localstorage");

const PROPS_TO_SAVE = ['id', 'activeStory', 'activeStoryMessage'];

export class UserModel extends Model {
    id: string;
    paid: boolean = false;


    get firstVisit() {
        return localStorage.getItem('first_visit') !== false;
    }

    set firstVisit(value) {
        localStorage.setItemObject('first_visit', value);
    }

    get activeStory() {
        return localStorage.getItem('active_story');
    }

    set activeStory(value) {
        localStorage.setItem('active_story', value);
    }

    getActiveStoryMessage(story, episode) {
        return localStorage.getItem(`story_${story}_episode_${episode}_message`) || 0;
    }

    setActiveStoryMessage(story, episode, index) {
        localStorage.setItem(`story_${story}_episode_${episode}_message`, index);
    }

    getActiveStoryEpisode(story) {
        return localStorage.getItem(`${story}_episode`);
    }

    setActiveStoryEpisode(story, id) {
        localStorage.setItem(`${story}_episode`, id);
    }

    save() {
        localStorage.setItemObject('user', this.serialize());
    }

    load() {
        this.deserialize(localStorage.getItem('user'));
    }

    deserialize(data: any = {}): UserModel {
        PROPS_TO_SAVE.forEach((item) => this[item] = data[item]);
        return this;
    }

    serialize(): any {
        return PROPS_TO_SAVE.reduce((result, item) => {
            return Object.assign(result, {
                [item]: this[item]
            })
        }, {});
    }
}