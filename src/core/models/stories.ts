const stories: any = [
    {
        "id": "1",
        "title": "The story about ancient vikings",
        "description": "Description or less important information. Maybe about 300 symbol and line...Continue reading",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/034/original/photo-1441155472722-d17942a2b76a?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=36709a54f34cad238516a5295bc7333a",
        "updatedAt": "",
        "category": "mistic",
        "estimate": 5,
        "author": "Artem Chehowsky",
        "messages": [
            {
                "id": "1",
                "index": 0,
                "author": "Artem",
                "body": "Hey, my dear friend!",
            },
            {
                "id": "2",
                "index": 1,
                "author": "Dima",
                "body": "Hell, Artem",
                "contentType": "image",
                "contentUrl":"https://placemat.imgix.net/placeholder_images/images/000/000/051/original/photo-1441966055611-30ea468880a2?ixlib=rb-1.0.0&w=600&h=950&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=e953676dabc4eb802a2c04ac8392f4b7"
            },
            {
                "id": "3",
                "index": 2,
                "author": "Artem",
                "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis eros quis faucibus lobortis. Vestibulum commodo vestibulum consequat. Nunc fringilla varius neque vitae varius. Mauris interdum vitae risus vel pellentesque. Duis consequat vitae sem vitae porttitor. Integer feugiat viverra vulputate. Donec ultricies massa ac augue ornare, sed feugiat augue pretium. Nunc dictum velit ac sem tincidunt, et tempor arcu accumsan. Aenean tempus vehicula sapien sit amet efficitur. Vestibulum sollicitudin, elit interdum tristique consectetur, mauris orci porttitor mi, in porttitor dui turpis ac dolor. "
            }
        ]
    },
    {
        "id": "2",
        "title": "Sexy chat...",
        "description": "Very interesting story. You should read it.",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/036/original/photo-1439546743462-802cabef8e97?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=f2caee62294766dee9c7d3b08e4cd1d0",
        "updatedAt": "",
        "category": "mistic",
        "messages": [
            {
                "id": "1",
                "index": 0,
                "author": "Artem",
                "body": "Hey, my dear friend!"
            },
            {
                "id": "2",
                "index": 1,
                "author": "Dima",
                "body": "Hell, Artem"
            },
            {
                "id": "3",
                "index": 2,
                "author": "Artem",
                "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis eros quis faucibus lobortis. Vestibulum commodo vestibulum consequat. Nunc fringilla varius neque vitae varius. Mauris interdum vitae risus vel pellentesque. Duis consequat vitae sem vitae porttitor. Integer feugiat viverra vulputate. Donec ultricies massa ac augue ornare, sed feugiat augue pretium. Nunc dictum velit ac sem tincidunt, et tempor arcu accumsan. Aenean tempus vehicula sapien sit amet efficitur. Vestibulum sollicitudin, elit interdum tristique consectetur, mauris orci porttitor mi, in porttitor dui turpis ac dolor. "
            }
        ]
    },
    {
        "id": "3",
        "title": "Sexy chat...",
        "description": "Very interesting story. You should read it.",
        "image": "https://placem.at/places?random=1&w=500&h=900&txt=0",
        "updatedAt": "",
        "category": "mistic",
        "estimate": 5,
        "messages": [
            {
                "id": "1",
                "index": 0,
                "author": "Artem",
                "body": "Hey, my dear friend!"
            },
            {
                "id": "2",
                "index": 1,
                "author": "Dima",
                "body": "Hell, Artem"
            },
            {
                "id": "3",
                "index": 2,
                "author": "Artem",
                "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis eros quis faucibus lobortis. Vestibulum commodo vestibulum consequat. Nunc fringilla varius neque vitae varius. Mauris interdum vitae risus vel pellentesque. Duis consequat vitae sem vitae porttitor. Integer feugiat viverra vulputate. Donec ultricies massa ac augue ornare, sed feugiat augue pretium. Nunc dictum velit ac sem tincidunt, et tempor arcu accumsan. Aenean tempus vehicula sapien sit amet efficitur. Vestibulum sollicitudin, elit interdum tristique consectetur, mauris orci porttitor mi, in porttitor dui turpis ac dolor. "
            }
        ]
    },
    {
        "id": "4",
        "title": "Sexy chat...",
        "description": "Very interesting story. You should read it.",
        "image": "https://placemat.imgix.net/placeholder_images/images/000/000/039/original/photo-1439694458393-78ecf14da7f9?ixlib=rb-1.0.0&w=500&h=900&fm=auto&crop=faces%2Centropy&fit=crop&txt=&txtclr=BFFF&txtalign=middle%2Ccenter&txtfit=max&txtsize=42&txtfont=Avenir+Next+Demi%2CBold&bm=multiply&blend=ACACAC&s=fd335fe2e88280ce3439468dbf49f7f0",
        "updatedAt": "",
        "category": "mistic",
        "estimate": 5,
        "messages": [
            {
                "id": "1",
                "index": 0,
                "author": "Artem",
                "body": "Hey, my dear friend!"
            },
            {
                "id": "2",
                "index": 1,
                "author": "Dima",
                "body": "Hell, Artem"
            },
            {
                "id": "3",
                "index": 2,
                "author": "Artem",
                "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis eros quis faucibus lobortis. Vestibulum commodo vestibulum consequat. Nunc fringilla varius neque vitae varius. Mauris interdum vitae risus vel pellentesque. Duis consequat vitae sem vitae porttitor. Integer feugiat viverra vulputate. Donec ultricies massa ac augue ornare, sed feugiat augue pretium. Nunc dictum velit ac sem tincidunt, et tempor arcu accumsan. Aenean tempus vehicula sapien sit amet efficitur. Vestibulum sollicitudin, elit interdum tristique consectetur, mauris orci porttitor mi, in porttitor dui turpis ac dolor. "
            }
        ]
    }
];
export default stories;