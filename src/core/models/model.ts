
export interface IModel {
    deserialize(data: any): void
}

export class Model implements IModel {
    deserialize(data: Object): Model {
        Object.assign(this, data);
        return this;
    }
}