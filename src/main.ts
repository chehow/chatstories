// this import should be first in order to load some required settings (like globals and reflect-metadata)
import {platformNativeScriptDynamic} from "nativescript-angular/platform";
import {enableProdMode} from '@angular/core';
import {registerElement} from "nativescript-angular/element-registry";
import {Video} from 'nativescript-exoplayer';
import {AppModule} from "./app/app.module";
import {CardView} from "nativescript-cardview";
import {Gif} from "nativescript-gif";


registerElement("Ripple", () => require("nativescript-ripple").Ripple);
registerElement('CardView', () => CardView);
registerElement("VideoPlayer", () => Video);
registerElement("Gif", () => Gif);

enableProdMode();
platformNativeScriptDynamic().bootstrapModule(AppModule);

