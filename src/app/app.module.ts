import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {NativeScriptUISideDrawerModule} from "nativescript-ui-sidedrawer/angular";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";
import {NativeScriptCommonModule} from "nativescript-angular/common";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {NativescriptBottomNavigationModule} from "nativescript-bottom-navigation/angular";
import {StoriesService} from "~/core/services/stories.service";
import {ApiService} from "~/core/services/api.service";
import {HttpClient} from "@angular/common/http";
import {HttpClientModule} from "@angular/common/http";
import {NgShadowModule} from 'nativescript-ngx-shadow';
import {StoryPreviewComponent} from "~/app/home/story-preview/story-preview.component";
import {StoriesListComponent} from "~/app/home/stories-list/stories-list.component";
import {StoryChatComponent} from "~/app/home/story-chat/story-chat.component";
import {StoryCardComponent} from "~/app/home/story-card/story-card.component";
import {MediaPreviewComponent} from './home/media-preview/media-preview.component';
import {ModalDialogService} from "nativescript-angular/modal-dialog";
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {CategoriesComponent} from './home/categories/categories.component';
import {UsersService} from "~/core/services/users.service";
import {SubscriptionComponent} from './home/subscription/subscription.component';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NativeScriptCommonModule,
        NativescriptBottomNavigationModule,
        NgShadowModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        AppComponent,
        StoryPreviewComponent,
        StoriesListComponent,
        StoryChatComponent,
        StoryCardComponent,
        MediaPreviewComponent,
        CategoriesComponent,
        SubscriptionComponent
    ],
    providers: [
        HttpClient,
        ApiService,
        UsersService,
        StoriesService,
        ModalDialogService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {
}
