import {Component, OnInit, AfterViewInit} from "@angular/core";
import {DrawerTransitionBase, SlideInOnTopTransition} from "nativescript-ui-sidedrawer";
import stories from "~/stories/stories";
import {initializeOnAngular} from "nativescript-web-image-cache";
import {RouterExtensions} from "nativescript-angular";
import {UsersService} from "~/core/services/users.service";
import {UserModel} from "~/core/models/user.model";

const firebase = require("nativescript-plugin-firebase");
const firebaseApp = require("nativescript-plugin-firebase/app");

const DEFAULT_STORY = '1';

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit, AfterViewInit {
    private _sideDrawerTransition: DrawerTransitionBase;
    private storiesCollection;
    private user: UserModel

    constructor(
        private routerExtensions: RouterExtensions,
        private userService: UsersService
    ) {
        this.user = userService.getUser();
        initializeOnAngular();
    }

    async ngOnInit(): Promise<any> {
        this._sideDrawerTransition = new SlideInOnTopTransition();
        await firebase.init({});

        if (this.user.firstVisit) {
            this.user.firstVisit = false;
            this.routerExtensions.navigate([`/stories/${DEFAULT_STORY}/episodes/default`]);
        }
        this.storiesCollection = firebaseApp.firestore().collection("stories");
    }

    ngAfterViewInit() {

    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    updateStories() {
        Promise.all(stories.map(story => {
            this.storiesCollection.doc(story.id).set(story);
        })).then(
            (result) => alert('Stories updated!'),
            (error) => alert('Error! Story do not updated!')
        );
    }
}
