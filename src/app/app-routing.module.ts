import {NgModule} from "@angular/core";
import {Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {StoriesListComponent} from "~/app/home/stories-list/stories-list.component";
import {StoryPreviewComponent} from "~/app/home/story-preview/story-preview.component";
import {StoryChatComponent} from "~/app/home/story-chat/story-chat.component";
import {MediaPreviewComponent} from "./home/media-preview/media-preview.component";
import {CategoriesComponent} from "./home/categories/categories.component";
import {SubscriptionComponent} from "~/app/home/subscription/subscription.component";

const routes: Routes = [
    {path: "", redirectTo: "/stories", pathMatch: "full"},
    {
        path: "stories", children: [
            {path: "", component: StoriesListComponent},
            {path: "categories", component: CategoriesComponent},
            {path: ":id/preview", component: StoryPreviewComponent},
            {path: ":id/episodes/:episode_id", component: StoryChatComponent},
            {path: "media-preview-modal", component: MediaPreviewComponent},
            {path: "subscription-modal", component: SubscriptionComponent},
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
