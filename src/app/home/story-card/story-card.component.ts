import {Component, Input, OnInit} from '@angular/core';
import {StoryModel} from "~/core/models/story.model";

@Component({
    selector: 'story-card',
    templateUrl: './story-card.component.html',
    styleUrls: ['./story-card.component.css'],
    moduleId: module.id
})
export class StoryCardComponent implements OnInit {
    @Input() story: StoryModel;

    constructor() {
    }

    ngOnInit() {
    }
}
