import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalDialogParams} from "nativescript-angular";
import {UsersService} from "~/core/services/users.service";
import {UserModel} from "~/core/models/user.model";

@Component({
    moduleId: module.id,
    selector: 'subscription',
    templateUrl: './subscription.component.html',
    styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit, OnDestroy {
    private user: UserModel;

    constructor(private params: ModalDialogParams, private userService: UsersService) {
        this.user = this.userService.getUser();
    }

    ngOnInit(): void {
    }

    ngOnDestroy() {
    }

    close(): void {
        this.user.paid = true;
        this.params.closeCallback();
    }
}