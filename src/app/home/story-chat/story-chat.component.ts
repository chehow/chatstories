import {
    AfterViewInit, ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnInit, ViewContainerRef
} from "@angular/core";
import {StoryModel} from "~/core/models/story.model";
import {ActivatedRoute} from "@angular/router";
import {StoriesService} from "~/core/services/stories.service";
import {StoryMessageModel} from "~/core/models/story-message.model";
import {ViewChild} from "@angular/core";
import {ItemEventData, ListView} from 'ui/list-view';
import {ModalDialogOptions, ModalDialogService, PageRoute, RouterExtensions} from "nativescript-angular";
import {isAndroid, isIOS, Page} from "tns-core-modules/ui/page";
import {ObservableArray} from "tns-core-modules/data/observable-array";
import {switchMap} from "rxjs/operators";
import {MediaPreviewComponent} from "~/app/home/media-preview/media-preview.component";
import {UserModel} from "~/core/models/user.model";
import {UsersService} from "~/core/services/users.service";
import {SubscriptionComponent} from "~/app/home/subscription/subscription.component";

const debounce = require('lodash/throttle');

declare var UITableViewCellSelectionStyle;
const MESSAGE_THROTTLE = 500;
const common = require('common-prefix');
const NEW_LETTER_DELAY = 50;
const MESSAGE_REMOVE_DELAY = 200;

@Component({
    selector: 'story-chat',
    moduleId: module.id,
    templateUrl: './story-chat.component.html',
    styleUrls: ['./story-chat.component.css'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class StoryChatComponent implements OnInit, AfterViewInit {
    @ViewChild('messagesList') listViewElem: ElementRef;
    story: StoryModel;
    listItems: ObservableArray<any> = new ObservableArray();
    messages: StoryMessageModel[];
    id: string;
    bg: string;
    list: ListView;
    public progressValue: number = 0;
    public finished: boolean = false;
    public isBusy: boolean = true;
    public user: UserModel;
    private debouncedTap: Function;
    private removeInterval: any;
    private addInterval: any;
    private timeout: any;
    private episode: string;

    constructor(
        private route: ActivatedRoute,
        private pageRoute: PageRoute,
        private storiesService: StoriesService,
        private routerExtensions: RouterExtensions,
        private page: Page,
        private cd: ChangeDetectorRef,
        private modalService: ModalDialogService,
        private _vcRef: ViewContainerRef,
        private userService: UsersService
    ) {
        this.user = this.userService.getUser();
        this.debouncedTap = debounce(() => {
            this.onTap();
        }, MESSAGE_THROTTLE);

        this.page.actionBarHidden = true;
        this.pageRoute.activatedRoute
            .pipe(switchMap(activatedRoute => activatedRoute.params))
            .forEach((params) => {
                this.id = params["id"];
                this.episode = params["episode_id"];
            });
    }

    ngOnInit() {
    }

    async ngAfterViewInit() {
        this.setList();
        this.user.activeStory = this.id;
        this.story = await this.storiesService.getItem(this.id);
        this.bg = this.story.image;
        if (!this.episode || this.episode === 'default') {
            this.episode = this.story.episodes[0].id;
        }
        this.user.setActiveStoryEpisode(this.story.id, this.episode);
        this.messages = this.story.episodes.find(item => item.id === this.episode).messages;
        let defaultIndex = 0;//this.user.getActiveStoryMessage(this.story.id, this.episode);
        if (defaultIndex) {
            this.listItems = new ObservableArray(this.messages.slice(0, defaultIndex));
            this.listItems.getItem(this.listItems.length - 1).isActive = true;
        } else {
            this.addNextItem();
        }
        this.showLastMessage(false);
        this.setBgImage();
        this.isBusy = false;
        this.cd.detectChanges();
    }

    private setList() {
        this.list = this.listViewElem.nativeElement;
        if (isIOS) {
            this.list.iosEstimatedRowHeight = 0;
        }
    }

    onItemLoading(args: ItemEventData) {
        let itemModel = <StoryMessageModel>this.listItems.getItem(args.index);
        if (itemModel.type === 'editMessage' && !itemModel.started && !itemModel.stop) {
            if (itemModel.isActive) {
                itemModel.started = true;
                this.showDynamicMessage(itemModel);
            } else {
                this.checkStop(itemModel);
            }
        }
        if (isIOS) {
            const iosCell = args.ios;
            iosCell.selectionStyle = UITableViewCellSelectionStyle.None;
        }
    }

    private showDynamicMessage(itemModel, index = 0) {
        let message = itemModel.message[index];

        if (!message) {
            return;
        }

        this.removeInterval = setInterval(() => {
            itemModel.currentMessage += message.substr(itemModel.currentMessage.length, 1);
            this.cd.detectChanges();
            if (message.length === itemModel.currentMessage.length) {
                clearInterval(this.removeInterval);
                let nextMessage = itemModel.message[index + 1];
                if (nextMessage) {
                    this.timeout = setTimeout(() => {
                        this.addInterval = setInterval(() => {
                            itemModel.currentMessage = itemModel.currentMessage.substr(0, itemModel.currentMessage.length - 1);
                            this.cd.detectChanges();

                            if (!itemModel.currentMessage.length || common([message, nextMessage]) === itemModel.currentMessage) {
                                this.showDynamicMessage(itemModel, ++index);
                                clearInterval(this.addInterval);
                            }
                        }, NEW_LETTER_DELAY);

                    }, MESSAGE_REMOVE_DELAY);
                }
            }
        }, NEW_LETTER_DELAY)
    }

    checkStop(itemModel) {
        clearInterval(this.removeInterval);
        clearInterval(this.addInterval);
        clearTimeout(this.timeout);
        itemModel.currentMessage = itemModel.message[itemModel.message.length - 1];
        this.cd.detectChanges();
    }

    async onTap() {
        if (this.finished) {
            return;
        }

        if (isIOS) {
            await this.sleep();
        }

        if (this.messages.find(item => item.preview)) {
            return;
        }

        let active = this.getActive();
        if (active && active.type === 'editMessage') {
            active.stop = true;
            this.checkStop(active);
        }

        if (this.storyFinished()) {
            this.finished = true;
            this.clearActive();
            this.listItems.push({type: 'footer'});
            this.showLastMessage();
        } else {
            this.addNextItem();
            this.showLastMessage();
            this.setBgImage();
            this.user.setActiveStoryMessage(this.story.id, this.episode, this.listItems.length);
            this.progressValue = Math.round(this.listItems.length / this.messages.length * 100);
        }

        this.cd.detectChanges();
    }

    sleep(sleepTime = 0) {
        return new Promise((resolve) => {
            setTimeout(resolve, sleepTime);
        });
    }

    setBgImage() {
        let bg = this.getActive().backgroundImage;
        if (bg) {
            if (bg === 'preview') {
                this.bg = this.story.image;
            } else {
                this.bg = bg;
            }
        }
    }

    private getActive(): StoryMessageModel {
        return this.messages.find(item => item.isActive);
    }

    private async showLastMessage(animated: boolean = true) {
        if (isAndroid && animated) {
            await this.sleep(50);
        }

        let index = this.listItems.length - 1;
        if (animated) {
            this.list.scrollToIndexAnimated(index);
        } else {
            this.list.scrollToIndex(index);
        }
    }

    public goBack() {
        if (this.routerExtensions.canGoBackToPreviousPage()) {
            this.routerExtensions.backToPreviousPage();
        } else {
            this.routerExtensions.navigate(['/stories']);
        }
    }

    public templateSelector(item: any) {
        return item.type;
    }

    private addNextItem() {
        this.clearActive();
        let item = this.messages[this.listItems.length];
        item.isActive = true;
        this.listItems.push(this.messages[this.listItems.length]);
    }

    clearActive() {
        this.listItems.forEach(item => {
            item.isActive = false;
        });
    }

    nextStory() {
        return this.story;
    }

    storyFinished() {
        return this.listItems.length >= this.messages.length;
    }

    messageClass(message: StoryMessageModel): string {
        let css = ['massage-wrapper'];

        if (message.isActive) {
            css.push('last-message');
        }

        if (message.type === 'info') {
            css.push('center');
        } else {
            css.push(message.align > 0 ? 'right' : 'left');
        }

        return css.join(' ');
    }

    previewMediaMessage(message) {
        message.preview = true;
        let modal: any = MediaPreviewComponent;
        let context = {
            message
        };

        if (!this.user.paid) {
            modal = SubscriptionComponent;
            Object.assign(context, {
                background: this.story.image,
            })
        }

        const options: ModalDialogOptions = {
            viewContainerRef: this._vcRef,
            animated: true,
            context,
            fullscreen: true
        };

        this.modalService.showModal(modal, options).then(() => {
            message.preview = false;
            this.cd.detectChanges();
        });
    }
}