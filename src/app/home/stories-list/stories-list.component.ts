import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {StoryModel} from "~/core/models/story.model";
import {StoriesService} from "~/core/services/stories.service";
import {ListView} from "tns-core-modules/ui/list-view";
import {BottomNavigation, OnTabPressedEventData, OnTabSelectedEventData} from "nativescript-bottom-navigation";
import {RadSideDrawer} from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {Page} from "tns-core-modules/ui/page";
import {RouterExtensions} from "nativescript-angular";

@Component({
    selector: "StoriesList",
    moduleId: module.id,
    styleUrls: ['./stories-list.component.css'],
    templateUrl: "./stories-list.component.html"
})
export class StoriesListComponent implements OnInit {
    @ViewChild("list") listViewElem: ElementRef;
    stories: StoryModel[];
    list: ListView;
    public selectedTab: number = 0;
    public isBusy: boolean = true;
    private _bottomNavigation: BottomNavigation;

    constructor(private storiesService: StoriesService, private page: Page, private routerExtensions: RouterExtensions) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this._bottomNavigation = this.page.getViewById('bottomNavigation');
        this.getStories();
    }


    private async getStories() {
        this.stories = await this.storiesService.getList();
        this.isBusy = false;
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onBottomNavigationTabSelected(args: OnTabSelectedEventData): void {
        this.selectedTab = args.newIndex;

        if (this.selectedTab === 0) {
            this.navigateToHomePage();
        }

        if (this.selectedTab === 1) {
            this.navigateToCategoriesPage();
        }
    }

    onBottomNavigationTabPressed(args: OnTabPressedEventData): void {
        if (args.index === 2) {
            this.onDrawerButtonTap();
        }
    }

    private navigateToHomePage() {
        return this.routerExtensions.navigate([""]);
    }

    private navigateToCategoriesPage() {

    }
}
