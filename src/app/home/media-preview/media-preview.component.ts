import {
    ChangeDetectionStrategy,
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
    ElementRef,
    ChangeDetectorRef
} from '@angular/core';
import {ModalDialogParams} from "nativescript-angular/modal-dialog";
import {StoryMessageModel} from '~/core/models/story-message.model';
import {Page} from "tns-core-modules/ui/page";

@Component({
    moduleId: module.id,
    selector: 'media-preview',
    templateUrl: './media-preview.component.html',
    styleUrls: ['./media-preview.component.css'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class MediaPreviewComponent implements OnInit, OnDestroy {
    @ViewChild("video") videoElem: ElementRef;
    private message: StoryMessageModel;

    constructor(private params: ModalDialogParams) {
        this.message = this.params.context.message;
    }

    ngOnInit(): void {
    }

    ngOnDestroy() {
        if (this.videoElem) {
            this.videoElem.nativeElement.destroy();
        }
    }

    close(): void {
        this.params.closeCallback();
    }
}
