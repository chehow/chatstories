import {Component, OnInit} from '@angular/core';
import {StoriesService} from "~/core/services/stories.service";
import {Page} from "tns-core-modules/ui/page";
import {StoryModel} from "~/core/models/story.model";
import {CategoryModel} from "~/core/models/category.model";
const groupBy = require("lodash/groupBy");

@Component({
    moduleId: module.id,
    selector: 'Categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
    public stories: StoryModel[];
    public categories: CategoryModel[];
    public isBusy: boolean = true;

    constructor(private storiesService: StoriesService, private page: Page) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.getStories();
    }

    private async getStories() {
        this.stories = await this.storiesService.getList();

        let categories = groupBy(this.stories, story => {
            return story.category;
        });

        this.categories = Object.keys(categories).map(category => {
            let categoryModel = new CategoryModel();
            categoryModel.deserialize({
                id: category,
                title: category,
                stories: categories[category]
            });
            return categoryModel;
        });

        this.isBusy = false;
    }
}
