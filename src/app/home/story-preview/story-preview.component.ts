import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {StoryModel} from "~/core/models/story.model";
import {ActivatedRoute} from "@angular/router";
import {StoriesService} from "~/core/services/stories.service";
import {Page} from "tns-core-modules/ui/page";
import {switchMap} from "rxjs/operators";
import {PageRoute, RouterExtensions} from "nativescript-angular";
import {UsersService} from "~/core/services/users.service";
import {UserModel} from "~/core/models/user.model";

@Component({
    selector: "StoryPreview",
    moduleId: module.id,
    styleUrls: ['./story-preview.component.css', './../story-card/story-card.component.css'],
    templateUrl: "./story-preview.component.html"
})
export class StoryPreviewComponent implements OnInit {
    story: StoryModel;
    id: string;
    progress: number = 0;
    user: UserModel;
    activeEpisode: string;

    constructor(
        private route: ActivatedRoute,
        private storiesService: StoriesService,
        private pageRoute: PageRoute,
        private routerExtensions: RouterExtensions,
        private page: Page,
        private userService: UsersService,
        private cd: ChangeDetectorRef
    ) {
        this.pageRoute.activatedRoute
            .pipe(switchMap(activatedRoute => activatedRoute.params))
            .forEach((params) => this.id = params["id"]);
        this.page.actionBarHidden = true;
        this.user = this.userService.getUser();

    }

    async ngOnInit(): Promise<any> {
        this.story = await this.storiesService.getItemInfo(this.id);
        this.updateProgress();
        this.page.on('navigatedTo', () => this.updateProgress())
    }

    updateProgress() {
        this.activeEpisode = this.user.getActiveStoryEpisode(this.id);
        this.progress = this.user.getActiveStoryMessage(this.id, this.activeEpisode) || 0;
        this.cd.detectChanges();
    }

    isActiveEpisode(episode) {
        return this.activeEpisode === episode.id;
    }

    public goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
